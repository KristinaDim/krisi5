package shopPragmatic;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestActionCar {
    WebDriver driver;

    @BeforeMethod
    public void setDriver(){
        System.setProperty("webdriver.chrome.driver","C:\\WebDrivers\\1Chrom\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://pragmatic.bg/automation/lecture13/Config.html");
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void setColors(){
        Select allColors = new Select(driver.findElement(By.name("color")));
        Actions builder = new Actions(driver);
        builder
                .keyDown(Keys.CONTROL)
                .click(driver.findElement(By.cssSelector("select[name='color'] option[value='rd']")))
                .click(driver.findElement(By.cssSelector("select[name='color'] option[value='sl']")))
                .perform();
        Assert.assertEquals(allColors.getAllSelectedOptions().size(), 2);
        List<String> exp_opt = Arrays.asList(new String[]{"Red", "Silver"});
        List<String> act_opt = new ArrayList<String>();
        for (WebElement options:allColors.getAllSelectedOptions()) {
            act_opt.add(options.getText());
        }
        Assert.assertEquals(act_opt,exp_opt);
    }

}
