package shopPragmatic;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

public class TestShopPragmatic {
    WebDriver driver;

    @BeforeMethod
    public void setDriver(){
        System.setProperty("webdriver.chrome.driver","C:\\WebDrivers\\1Chrom\\chromedriver.exe");
        driver = new ChromeDriver();

//        System.setProperty("webdriver.ie.driver", "C:\\WebDrivers\\4IE\\IEDriverServer.exe");
//        driver = new InternetExplorerDriver();

//        System.setProperty("webdriver.gecko.driver", "C:\\WebDrivers\\2FireFox\\geckodriver.exe");
//        driver = new FirefoxDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/admin/");
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void loginTestCorrect() throws InterruptedException {
        WebElement userName = driver.findElement(By.id("input-username"));
        userName.sendKeys("admin");
        WebElement password = driver.findElement(By.id("input-password"));
        password.sendKeys("parola123!");
        driver.findElement(By.cssSelector("button.btn")).click();

        WebElement element = driver.findElement(By.id("user-profile"));
        String alt = element.getAttribute("alt");
        Assert.assertEquals("Milen Strahinski",alt);

        driver.findElement(By.cssSelector("span.hidden-xs")).click();
        Thread.sleep(5000);
    }

    @Test
    public void loginTestFail1() throws InterruptedException {
        WebElement userName = driver.findElement(By.id("input-username"));
        userName.sendKeys("Kristina");
        WebElement password = driver.findElement(By.id("input-password"));
        password.sendKeys("parola123!");
        driver.findElement(By.cssSelector("button.btn")).click();


        WebElement element = driver.findElement(By.cssSelector("button.close"));
        String cl = element.getAttribute("class");
        Assert.assertEquals("close",cl);
//        WebElement element = driver.findElement(By.cssSelector("div.alert"));
//        String cl = element.getAttribute("class");
//        Assert.assertEquals("alert alert-danger alert-dismissible", cl);

        Thread.sleep(5000);
    }

    @Test
    public void loginTestFail2() throws InterruptedException {
        WebElement userName = driver.findElement(By.id("input-username"));
        userName.sendKeys("admin");
        WebElement password = driver.findElement(By.id("input-password"));
        password.sendKeys("Kristina");
        driver.findElement(By.cssSelector("button.btn")).click();

        WebElement element = driver.findElement(By.cssSelector("div.alert"));
        String cl = element.getAttribute("class");
        Assert.assertEquals("alert alert-danger alert-dismissible", cl);

        Thread.sleep(5000);
    }

    @Test
    public void loginTestFail3() throws InterruptedException {
        WebElement userName = driver.findElement(By.id("input-username"));
        userName.sendKeys("Kristina");
        WebElement password = driver.findElement(By.id("input-password"));
        password.sendKeys("Kristina");
        driver.findElement(By.cssSelector("button.btn")).click();

        WebElement element = driver.findElement(By.cssSelector("div.alert"));
        String cl = element.getAttribute("class");
        Assert.assertEquals("alert alert-danger alert-dismissible", cl);

        Thread.sleep(5000);

    }

    @Test
    public void loginFail() throws InterruptedException {
        WebElement userName = driver.findElement(By.id("input-username"));
        userName.sendKeys("Kristina");
        WebElement password = driver.findElement(By.id("input-password"));
        password.sendKeys("Kristina");
        driver.findElement(By.cssSelector("button.btn")).click();
        driver.findElement(By.xpath("//input[@id='input-username']")).clear();
        driver.findElement(By.xpath("//input[@id='input-password']")).clear();
        WebElement userName1 = driver.findElement(By.xpath("//input[@id='input-username']"));
        userName1.sendKeys("admin");
        WebElement password1 = driver.findElement(By.xpath("//input[@id='input-password']"));
        password1.sendKeys("Kristina");
        driver.findElement(By.cssSelector("button.btn")).click();
        driver.findElement(By.xpath("//input[@id='input-username']")).clear();
        driver.findElement(By.xpath("//input[@id='input-password']")).clear();
        WebElement userName2 = driver.findElement(By.xpath("//input[@id='input-username']"));
        userName2.sendKeys("Kristina");
        WebElement password2 = driver.findElement(By.xpath("//input[@id='input-password']"));
        password2.sendKeys("parola123!");
        driver.findElement(By.cssSelector("button.btn")).click();

        WebElement element = driver.findElement(By.cssSelector("div.alert"));
        String cl = element.getAttribute("class");
        Assert.assertEquals("alert alert-danger alert-dismissible", cl);

        Thread.sleep(5000);
    }

    @Test
    public void checkSalesOrders() throws InterruptedException {
        WebElement userName = driver.findElement(By.id("input-username"));
        userName.sendKeys("admin");
        WebElement password = driver.findElement(By.id("input-password"));
        password.sendKeys("parola123!");
        driver.findElement(By.cssSelector("button.btn")).click();

        driver.findElement(By.xpath("//i[@class='fa fa-shopping-cart fw']")).click();
        driver.findElement(By.xpath("//ul[@class='collapse in']//a[text()='Orders']")).click();

        WebElement orderStatus = driver.findElement(By.id("input-order-status"));
        Select order = new Select(orderStatus);

        assertFalse(order.isMultiple());
        assertEquals(order.getOptions().size(), 16);

        List<String> exp_options = Arrays.asList(new String[]{"", "Missing Orders", "Canceled", "Canceled Reversal",
                "Chargeback", "Complete", "Denied", "Expired", "Failed", "Pending", "Processed", "Processing", "Refunded",
        "Reversed", "Shipped", "Voided"});
        List<String> act_options = new ArrayList<String>();

        List<WebElement> allOptions = order.getOptions();
        for (WebElement options: allOptions) {
            act_options.add(options.getText());
        }

        Assert.assertEquals(act_options.toArray(), exp_options.toArray());

        order.selectByIndex(1);
        assertEquals(order.getFirstSelectedOption().getText(), "Missing Orders");
        order.selectByIndex(15);
        assertEquals(order.getFirstSelectedOption().getText(), "Voided");

        order.selectByValue("7");
        assertEquals(order.getFirstSelectedOption().getText(), "Canceled");
        order.selectByValue("3");
        assertEquals(order.getFirstSelectedOption().getText(), "Shipped");

        order.selectByVisibleText("Processing");
        assertEquals(order.getFirstSelectedOption().getText(),"Processing");
        order.selectByVisibleText("Processed");
        assertEquals(order.getFirstSelectedOption().getText(),"Processed");

    }

}
