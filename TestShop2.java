package shopPragmatic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestShop2 {
    WebDriver driver;

    @BeforeMethod
    public void setDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\WebDrivers\\1Chrom\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void loginTest() throws InterruptedException {
        WebElement myAccount = driver.findElement(By.xpath("//i[@class='fa fa-user']"));
        myAccount.click();
        WebElement login = driver.findElement(By.xpath("//ul[@class='dropdown-menu dropdown-menu-right']//li[2]"));
        login.click();

        WebElement element = driver.findElement(By.xpath("//input[@id='input-email']"));
        String email = element.getAttribute("id");
        Assert.assertEquals("input-email", email);
        Thread.sleep(3000);
    }

    @Test
    public void checkShow() throws InterruptedException {
        Actions builder = new Actions(driver);
        builder
                .click(driver.findElement(By.xpath("//a[text()='Desktops']")))
                .click(driver.findElement(By.xpath("//a[text()='Mac (1)']")))
//                .click(driver.findElement(By.xpath("//select[@id='input-limit']")))
                .perform();
        Select show;

        show = new Select(driver.findElement(By.xpath("//select[@id='input-limit']")));
        Assert.assertFalse(show.isMultiple());
        Assert.assertEquals(show.getOptions().size(), 5);

        Thread.sleep(5000);

        show.selectByIndex(0);
        Assert.assertEquals(show.getFirstSelectedOption().getText(), "15");



        show.selectByIndex(1);
        show = new Select(driver.findElement(By.xpath("//select[@id='input-limit']")));
        Assert.assertEquals(show.getFirstSelectedOption().getText(), "25");
//        show.selectByIndex(4);
//        Assert.assertEquals(show.getFirstSelectedOption().getText(),"100");
    }

}
